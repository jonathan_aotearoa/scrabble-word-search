# Scrabble Word Finder

A bit of fun with data structures and algorithms...

## Problem Overview

Find all the words the can be made from all or some of the letters in a given collection of letters.
The [SOWPODS](https://en.wikipedia.org/wiki/SOWPODS) scrabble lexicon contains 267,751 words.

## Approaches

There are a number of ways to solve this problem, including but not limited to:

### Brute Force

Simply iterate over every word in the lexicon and see if it can be constructed from the specified letters.

### Dictionary Based

Here, the term dictionary refers to a multimap. Keys are created by sorting the characters in a word lexicographically.
For example, 'ant', 'nat', and 'tan' all have the key 'ant'.
To find every word that can be created by all or some of the specified letters the following operations are performed:

1. A [power set](https://en.wikipedia.org/wiki/Power_set) of the specified letters is created
2. A set of dictionary keys is created by sorting the characters in each power set subset
3. The word(s) stored under each dictionary key are added to a single result set

#### Constructing a Power Set

Guava provides
a [utility method](http://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/collect/Sets.html#powerSet%28java.util.Set%29)
for creating a power set.
However, in the context of a scrabble word finder this implementation has a number of limitations:

* It only accepts an argument of type [Set](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html). A collection
  of Scrabble tiles may contain duplicate elements.
* There's no way of constraining the size of power set subsets, which is useful if you only want to find words of
  certain length.

As a result, I
created [my own power set construction method](https://bitbucket.org/jonathan_aotearoa/scrabble-word-search/src/b3387bd259e45d694d47f412150a20ee334daea7/src/main/java/jonathan/aotearoa/scrabble/search/DictionarySearch.java?at=master&fileviewer=file-view-default).

### Trie Based

This approach stores the lexicon in a [trie](https://en.wikipedia.org/wiki/Trie) data structure.
Each node has two properties; the character it represents and a boolean flag indicating whether it's a word end.
A recursive method is used to find all the words that can be constructed from a specific collection of characters.

This approach also lends itself to parallelization. For example, when searching for words that can be constructed from
the letters 'restful' one thread would search for all possible words starting with the letter 'r', another for all words
starting with the letter 'e', and so on.

## Results

## Initial Results

The following table shows the average execution time in milliseconds for each search implementation.
For each implementation, 1000 sequential searches were executed with the same 15 character input ("ocdfaestrghling",
which returns 8491 possible words between 2 and 15 characters in length).
The 10 slowest and 10 quickest results were then removed from the result set before calculating an average execution
time.

| Search               | Avg time (ms) |
|----------------------|---------------|
| Brute Force          | 160.5         |
| Dictionary           | 11.2          |
| Trie                 | 7.4           |
| Trie, multi-threaded | 4.6           |

## Post-refactor results

GraalVM CE 21 running on an AMD 5600G with 16GB of RAM.

| Search                      | Avg time (ms) |
|-----------------------------|---------------|
| Brute Force, multi-threaded | 29.0          |
| Dictionary                  | 3.0           |
| Trie                        | 4.0           |
| Trie, multi-threaded        | 3.1           |

### JMH Benchmark

```
Benchmark               (minLength)     (searchTerm)         (searchType)   Mode  Cnt    Score    Error  Units
SearchBenchmark.search            2  ocdfaestrghling          BRUTE_FORCE  thrpt    3   64.339 ±  1.054  ops/s
SearchBenchmark.search            2  ocdfaestrghling           DICTIONARY  thrpt    3  461.610 ±  6.052  ops/s
SearchBenchmark.search            2  ocdfaestrghling                 TRIE  thrpt    3  338.398 ± 19.638  ops/s
SearchBenchmark.search            2  ocdfaestrghling  MULTI_THREADED_TRIE  thrpt    3  792.822 ± 11.104  ops/s
```

## Screenshots

![Screenshot 1](https://bitbucket.org/jonathan_aotearoa/scrabble-word-search/downloads/scrabble-word-search-screenshot-1.png)