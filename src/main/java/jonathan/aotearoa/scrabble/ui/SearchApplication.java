package jonathan.aotearoa.scrabble.ui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * A very basic UI
 */
public class SearchApplication extends Application {

    private SearchModel model;

    @Override
    public void start(final Stage primaryStage) throws Exception {
        model = SearchModel.newDictionarySearchModel();
        primaryStage.setTitle("Scrabble Search");
        final GridPane gridPane = createControls();
        final Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private GridPane createControls() {
        final GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10));
        // controls
        final Label lettersLabel = new Label("Letters:");
        final TextField lettersTextField = new TextField();
        final Label minWordLengthLabel = new Label("Min length:");
        final Spinner<Integer> minWordLengthSpinner = new Spinner<>(2, 15, 5, 1);
        final ListView<String> resultsListView = new ListView<>(model.getResultsList());
        // actions
        minWordLengthSpinner.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.search(lettersTextField.getText(), newValue);
        });
        lettersTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            model.search(newValue, minWordLengthSpinner.getValue());
        });
        // add controls to pane
        gridPane.add(lettersLabel, 0, 0);
        gridPane.add(lettersTextField, 1, 0);
        gridPane.add(minWordLengthLabel, 0, 1);
        gridPane.add(minWordLengthSpinner, 1, 1);
        gridPane.add(resultsListView, 0, 2, 2, 1);
        return gridPane;
    }
}