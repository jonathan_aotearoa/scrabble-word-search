package jonathan.aotearoa.scrabble.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jonathan.aotearoa.scrabble.search.Search;
import jonathan.aotearoa.scrabble.search.SearchType;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.util.List;

import static jonathan.aotearoa.scrabble.search.SearchFactory.SOWPODS_SEARCH_FACTORY;

public class SearchModel {

    private final Search search;
    private final ObservableList<String> resultsList;

    public SearchModel(final Search search) {
        this.search = Validate.notNull(search, "search cannot be null");
        resultsList = FXCollections.observableArrayList();
    }

    public static SearchModel newTrieSearchModel() throws IOException {
        final Search search = SOWPODS_SEARCH_FACTORY.newSearch(SearchType.TRIE);
        return new SearchModel(search);
    }

    public static SearchModel newDictionarySearchModel() throws IOException {
        final Search search = SOWPODS_SEARCH_FACTORY.newSearch(SearchType.DICTIONARY);
        return new SearchModel(search);
    }

    public void search(final String searchTerm, final int minLength) {
        final List<String> searchResults = this.search.search(searchTerm, minLength);
        resultsList.setAll(searchResults);
    }

    public ObservableList<String> getResultsList() {
        return resultsList;
    }
}