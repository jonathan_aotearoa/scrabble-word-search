package jonathan.aotearoa.scrabble.data;

/**
 * Basic trie node
 */
public final class TrieNode {

    private static final int ALPHABET_LENGTH = 26;
    private static final int INDEX_OFFSET = 'a';

    private final TrieNode[] children;
    private String word;

    TrieNode() {
        children = new TrieNode[ALPHABET_LENGTH];
    }

    public boolean hasChild(char c) {
        return children[getChildIndex(c)] != null;
    }

    public TrieNode addChild(char c) {
        if (hasChild(c)) {
            final String errorMsg = "Child node already exists for '%s'".formatted(c);
            throw new IllegalArgumentException(errorMsg);
        }
        final TrieNode child = new TrieNode();
        children[getChildIndex(c)] = child;
        return child;
    }

    public TrieNode getChild(char c) {
        return children[getChildIndex(c)];
    }

    public boolean isWordEnd() {
        return word != null;
    }

    public String getWord() {
        return word;
    }

    void setWord(final String word) {
        this.word = word;
    }

    private static int getChildIndex(final char c) {
        return c - INDEX_OFFSET;
    }
}