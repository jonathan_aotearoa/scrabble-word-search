package jonathan.aotearoa.scrabble.data;

import java.util.*;
import java.util.function.Consumer;

import static jonathan.aotearoa.scrabble.util.StringUtils.sortedString;

/**
 * A line consumer that constructs a dictionary.
 * <p>
 * A dictionary is map. Each entry contains the following information:
 * <ul>
 *     <li>Key: A string of characters in ascending order, with a min length or 1 and max length 15.</li>
 *     <li>Entry: A non-empty set containing all the lexicon words that comprise the letters in the key.</li>
 * </ul>
 * </p>
 */
public class DictionaryBuilder implements Consumer<String> {

    private final Map<String, Set<String>> dictionary;

    public DictionaryBuilder() {
        dictionary = new HashMap<>();
    }

    @Override
    public void accept(final String line) {
        final String word = line.trim().toLowerCase();
        final String key = sortedString(word);
        dictionary.computeIfAbsent(key, k -> new HashSet<>()).add(word);
    }

    public Map<String, Set<String>> getDictionary() {
        return Collections.unmodifiableMap(dictionary);
    }
}