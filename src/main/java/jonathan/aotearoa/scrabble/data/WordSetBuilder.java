package jonathan.aotearoa.scrabble.data;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class WordSetBuilder implements Consumer<String> {

    private static final int N_WORDS = 267751;
    private final Set<String> words;

    public WordSetBuilder() {
        words = new HashSet<>(N_WORDS);
    }

    @Override
    public void accept(final String line) {
        final String word = line.trim().toLowerCase();
        words.add(word);
    }

    public Set<String> getWords() {
        return Collections.unmodifiableSet(words);
    }
}