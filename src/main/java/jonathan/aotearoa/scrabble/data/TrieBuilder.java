package jonathan.aotearoa.scrabble.data;

import java.util.function.Consumer;

public class TrieBuilder implements Consumer<String> {

    private final TrieNode rootNode;

    public TrieBuilder() {
        rootNode = new TrieNode();
    }

    @Override
    public void accept(final String line) {
        final String word = line.trim().toLowerCase();
        final char[] chars = word.toCharArray();
        TrieNode node = rootNode;
        for (final char c : chars) {
            if (node.hasChild(c)) {
                node = node.getChild(c);
            } else {
                node = node.addChild(c);
            }
        }
        node.setWord(word);
    }

    public TrieNode getRootNode() {
        return rootNode;
    }
}