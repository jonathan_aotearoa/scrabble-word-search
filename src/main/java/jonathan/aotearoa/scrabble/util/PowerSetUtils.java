package jonathan.aotearoa.scrabble.util;

import java.util.BitSet;
import java.util.stream.IntStream;

public class PowerSetUtils {

    private PowerSetUtils() {
    }

    /**
     * Get indices of the elements that comprise each the power set subset.
     *
     * @param setSize the size of the set we're constructing a power-set for.
     * @return a two-dimensional array containing the indices of the elements that comprise each the power set subset.
     */
    public static int[][] getSubsetIndices(final int setSize) {
        final int powerSetSize = (int) Math.pow(2, setSize);
        return IntStream.range(0, powerSetSize)
                .mapToObj(i -> BitSet.valueOf(new long[]{i}).stream().toArray())
                .toArray(int[][]::new);
    }
}
