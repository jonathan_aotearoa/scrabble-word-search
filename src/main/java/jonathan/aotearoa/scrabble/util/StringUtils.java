package jonathan.aotearoa.scrabble.util;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * String utility methods
 */
public class StringUtils {

    private StringUtils() {
    }

    public static char[] getCharsAt(final char[] sourceChars, final int[] indices) {
        Validate.notNull(sourceChars, "sourceChars cannot be null");
        Validate.notNull(indices, "indices cannot be null");

        final char[] resultChars = new char[indices.length];
        for (int i = 0; i < indices.length; i++) {
            resultChars[i] = sourceChars[indices[i]];
        }
        return resultChars;
    }

    public static char[] sortedChars(final String str) {
        Validate.notNull(str, "str cannot be null");

        final char[] chars = str.toCharArray();
        Arrays.sort(chars);
        return chars;
    }

    public static String sortedString(final String str) {
        final char[] chars = sortedChars(str);
        return String.valueOf(chars);
    }

    /**
     * Get a map containing the count for each distinct character in the specified string.
     * <p>
     * Note: Maps keys are character's integer value.
     * </p>
     *
     * @param str the string. Cannot be null.
     * @return a map containing the count for each distinct character in the specified string.
     */
    public static Map<Integer, Integer> getCharCounts(final String str) {
        Validate.notNull(str, "str cannot be null");

        final Map<Integer, Integer> charCounts = new HashMap<>();
        str.chars().forEach(charValue -> charCounts.merge(charValue, 1, Integer::sum));
        return Collections.unmodifiableMap(charCounts);
    }
}