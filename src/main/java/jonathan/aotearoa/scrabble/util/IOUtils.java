package jonathan.aotearoa.scrabble.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class IOUtils {

    public static final String SOWPODS_RESOURCE_NAME = "sowpods.txt";

    private IOUtils() {
    }

    public static void readLines(final String resourceName, final Consumer<String> lineConsumer) throws IOException {
        try (final InputStream in = getResource(resourceName)) {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            reader.lines().forEach(lineConsumer);
        }
    }

    private static InputStream getResource(final String resourceName) {
        final InputStream in = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(resourceName);
        if (in == null) {
            throw new IllegalArgumentException("Resource '%s' does not exist".formatted(resourceName));
        }
        return in;
    }
}