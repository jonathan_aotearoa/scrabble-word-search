package jonathan.aotearoa.scrabble.benchmark;

import jonathan.aotearoa.scrabble.search.Search;
import jonathan.aotearoa.scrabble.search.SearchType;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.List;

import static jonathan.aotearoa.scrabble.search.SearchFactory.SOWPODS_SEARCH_FACTORY;

@Fork(1)
@Warmup(iterations = 3, time = 5)
@Measurement(iterations = 3, time = 5)
@State(Scope.Thread)
public class SearchBenchmark {

    @Param({"ocdfaestrghling"})
    public String searchTerm;

    @Param({"2"})
    public int minLength;

    @Param({"BRUTE_FORCE", "DICTIONARY", "TRIE", "MULTI_THREADED_TRIE"})
    public SearchType searchType;

    private Search search;

    @Setup
    public void setUp() throws IOException {
        search = SOWPODS_SEARCH_FACTORY.newSearch(searchType);
    }

    @Benchmark
    public List<String> search() throws IOException {
        return search.search(searchTerm, minLength);
    }
}
