package jonathan.aotearoa.scrabble.search;

import jonathan.aotearoa.scrabble.data.DictionaryBuilder;
import jonathan.aotearoa.scrabble.data.TrieBuilder;
import jonathan.aotearoa.scrabble.data.TrieNode;
import jonathan.aotearoa.scrabble.data.WordSetBuilder;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import static jonathan.aotearoa.scrabble.util.IOUtils.SOWPODS_RESOURCE_NAME;
import static jonathan.aotearoa.scrabble.util.IOUtils.readLines;

/**
 * A factory for creating {@code Search}es
 */
public class SearchFactory {

    public static final SearchFactory SOWPODS_SEARCH_FACTORY = new SearchFactory(SOWPODS_RESOURCE_NAME);

    private final String lexiconResourceName;

    private SearchFactory(final String lexiconResourceName) {
        this.lexiconResourceName = lexiconResourceName;
    }

    /**
     * Creates a new {@code Search} instance
     *
     * @param searchType the type of search
     * @return a new {@code Search} instance of the specified type
     * @throws IOException if an error occurs reading the lexicon
     */
    public Search newSearch(final SearchType searchType) throws IOException {
        Validate.notNull(searchType, "searchType cannot be null");
        return switch (searchType) {
            case BRUTE_FORCE -> newBruteForceSearch();
            case DICTIONARY -> newDictionarySearch();
            case TRIE -> newTrieSearch();
            case MULTI_THREADED_TRIE -> newMultiThreadedTrieSearch();
        };
    }

    private Search newBruteForceSearch() throws IOException {
        final WordSetBuilder wordSetBuilder = new WordSetBuilder();
        readLines(this.lexiconResourceName, wordSetBuilder);
        final Set<String> words = wordSetBuilder.getWords();
        return new BruteForceSearch(words);
    }

    private Search newDictionarySearch() throws IOException {
        final DictionaryBuilder dictionaryBuilder = new DictionaryBuilder();
        readLines(this.lexiconResourceName, dictionaryBuilder);
        final Map<String, Set<String>> dictionary = dictionaryBuilder.getDictionary();
        return new DictionarySearch(dictionary);
    }

    private Search newTrieSearch() throws IOException {
        final TrieBuilder trieBuilder = new TrieBuilder();
        readLines(this.lexiconResourceName, trieBuilder);
        final TrieNode rootNode = trieBuilder.getRootNode();
        return new TrieSearch(rootNode);
    }

    private Search newMultiThreadedTrieSearch() throws IOException {
        final TrieBuilder trieBuilder = new TrieBuilder();
        readLines(this.lexiconResourceName, trieBuilder);
        final TrieNode rootNode = trieBuilder.getRootNode();
        return new MultiThreadedTrieSearch(rootNode);
    }
}