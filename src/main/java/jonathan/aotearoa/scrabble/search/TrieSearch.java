package jonathan.aotearoa.scrabble.search;

import jonathan.aotearoa.scrabble.data.TrieNode;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A word search based on a trie data structure
 */
final class TrieSearch extends Search {

    private final TrieNode rootNode;

    /**
     * Constructs a new {@code TrieSearch}
     *
     * @param rootNode the trie root node
     */
    public TrieSearch(final TrieNode rootNode) {
        super(SearchType.TRIE);
        this.rootNode = Validate.notNull(rootNode, "rootNode cannot be null");
    }

    @Override
    protected List<String> doSearch(final String letters, final int minLength) {
        final Set<String> results = new HashSet<>();
        final char[] available = letters.toCharArray();
        search(rootNode, available, minLength, results);
        return results.stream().sorted(ResultsComparator.getInstance()).collect(Collectors.toList());
    }

    /**
     * Recursive method for searching the trie
     *
     * @param node      the current node
     * @param available the list of available characters
     * @param minLength the minimum result word length
     * @param results   the set of results
     */
    static void search(final TrieNode node,
                       final char[] available,
                       final int minLength,
                       final Set<String> results) {
        if (node.isWordEnd() && node.getWord().length() >= minLength) {
            results.add(node.getWord());
        }
        for (int i = 0; i < available.length; i++) {
            final TrieNode childNode = node.getChild(available[i]);
            if (childNode == null) {
                continue;
            }
            final char[] nextAvailable = ArrayUtils.remove(available, i);
            search(childNode, nextAvailable, minLength, results);
        }
    }
}