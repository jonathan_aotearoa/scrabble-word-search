package jonathan.aotearoa.scrabble.search;

import jonathan.aotearoa.scrabble.util.PowerSetUtils;
import jonathan.aotearoa.scrabble.util.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptySet;

/**
 * Word search based on a dictionary data structure
 */
final class DictionarySearch extends Search {

    private final Map<String, Set<String>> dictionary;
    private final int[][] subsetsIndices;

    /**
     * Constructs a new {@code DictionarySearch}
     *
     * @param dictionary the dictionary that will be searched
     */
    public DictionarySearch(final Map<String, Set<String>> dictionary) {
        super(SearchType.DICTIONARY);
        this.dictionary = Validate.notNull(dictionary, "dictionary cannot be null");
        // One time operation rather than recalculating for every search
        subsetsIndices = PowerSetUtils.getSubsetIndices(MAX_WORD_LENGTH);
    }

    @Override
    protected List<String> doSearch(final String letters, final int minLength) {
        final char[] chars = letters.toCharArray();
        // Sort chars now to avoid sorting every key
        Arrays.sort(chars);
        // Construct power set using the set bits in integer values ranging from 1 to (2^n) - 1
        // where n is the number of letters.
        // We start a 1 because 0 represents an empty set, which we're not interested in.
        final int powerSetSize = (int) Math.pow(2, chars.length);

        return Arrays.stream(subsetsIndices, 1, powerSetSize)
                .parallel()
                .filter(subsetIndices -> subsetIndices.length >= minLength) // Ignore subsets smaller than minLength.
                .map(subsetIndices -> StringUtils.getCharsAt(chars, subsetIndices)) // Get the subset characters.
                .map(String::valueOf) // Subset chars as a string.
                .flatMap(dictionaryKey -> dictionary.getOrDefault(dictionaryKey, emptySet()).stream()) // Dictionary words.
                .distinct()
                .sorted(ResultsComparator.getInstance())
                .toList();
    }
}
