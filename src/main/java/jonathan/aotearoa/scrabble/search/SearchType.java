package jonathan.aotearoa.scrabble.search;

public enum SearchType {

    BRUTE_FORCE("Brute force"),
    DICTIONARY("Dictionary"),
    TRIE("Trie"),
    MULTI_THREADED_TRIE("Multi-threaded trie");

    public final String searchName;

    SearchType(final String searchName) {
        this.searchName = searchName;
    }
}
