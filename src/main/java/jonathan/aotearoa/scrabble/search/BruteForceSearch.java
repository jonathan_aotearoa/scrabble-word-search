package jonathan.aotearoa.scrabble.search;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import static jonathan.aotearoa.scrabble.util.StringUtils.getCharCounts;

final class BruteForceSearch extends Search {
    
    private final Set<String> words;

    public BruteForceSearch(final Set<String> words) {
        super(SearchType.BRUTE_FORCE);
        this.words = words;
    }

    @Override
    protected List<String> doSearch(final String letters, final int minLength) {
        final Predicate<String> predicate = new SearchPredicate(letters, minLength);
        return words.parallelStream()
                .filter(predicate)
                .sorted(ResultsComparator.getInstance())
                .toList();
    }

    private static class SearchPredicate implements Predicate<String> {

        private final int minLength;
        private final Map<Integer, Integer> letterCounts;

        private SearchPredicate(final String letters, final int minLength) {
            this.minLength = Math.min(minLength, letters.length());
            letterCounts = getCharCounts(letters);
        }

        @Override
        public boolean test(final String word) {
            return word.length() >= minLength && getCharCounts(word).entrySet()
                    .stream()
                    .allMatch(wordLetterCount -> hasLetters(wordLetterCount.getKey(), wordLetterCount.getValue()));
        }

        private boolean hasLetters(final Integer letter, final int count) {
            return letterCounts.getOrDefault(letter, 0) >= count;
        }
    }
}