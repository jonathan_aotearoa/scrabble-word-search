package jonathan.aotearoa.scrabble.search;


import jonathan.aotearoa.scrabble.data.TrieNode;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

final class MultiThreadedTrieSearch extends Search {

    private final TrieNode rootNode;

    public MultiThreadedTrieSearch(final TrieNode rootNode) {
        super(SearchType.MULTI_THREADED_TRIE);
        this.rootNode = Validate.notNull(rootNode, "rootNode cannot be null");
    }

    @Override
    protected List<String> doSearch(final String letters, final int minLength) {
        final char[] available = letters.toCharArray();
        return IntStream.range(0, available.length)
                .parallel()
                .mapToObj(i -> {
                    final TrieNode node = rootNode.getChild(available[i]);
                    if (node == null) {
                        return Collections.<String>emptySet();
                    }
                    final char[] nextAvailable = ArrayUtils.remove(available, i);
                    final Set<String> results = new HashSet<>();
                    TrieSearch.search(node, nextAvailable, minLength, results);
                    return results;
                })
                .flatMap(Set::stream)
                .distinct()
                .sorted(ResultsComparator.getInstance())
                .toList();
    }
}