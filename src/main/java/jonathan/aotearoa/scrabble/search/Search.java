package jonathan.aotearoa.scrabble.search;

import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.List;

/**
 * Word search
 */
public abstract class Search {

    /**
     * The minimum length of a word in Scrabble
     */
    public static int MIN_WORD_LENGTH = 2;

    /**
     * The maximum length of a word in Scrabble
     */
    public static int MAX_WORD_LENGTH = 15;

    public final SearchType type;

    protected Search(final SearchType type) {
        this.type = Validate.notNull(type, "type cannot be null");
    }

    /**
     * @param letters   the search letters
     * @param minLength the minimum length for result words
     * @return a list words that can be made from the letters in the specified {@code letters}
     */
    public final List<String> search(final String letters, final int minLength) {
        Validate.notNull(letters, "letters cannot be null");
        Validate.inclusiveBetween(MIN_WORD_LENGTH, MAX_WORD_LENGTH, minLength,
                "minLength %s must be between %s and %s inclusive", minLength, MIN_WORD_LENGTH, MAX_WORD_LENGTH);
        if (letters.length() < minLength) {
            return Collections.emptyList();
        }
        return doSearch(letters, minLength);
    }

    /**
     * Subclass search implementation
     *
     * @param letters   the search letters
     * @param minLength the minimum length for result words
     * @return a list words that can be made from the letters in the specified {@code letters}
     */
    protected abstract List<String> doSearch(String letters, int minLength);
}