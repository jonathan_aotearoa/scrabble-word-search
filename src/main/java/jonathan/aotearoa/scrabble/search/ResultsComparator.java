package jonathan.aotearoa.scrabble.search;

import java.util.Comparator;

/**
 * Comparator for sorting search results by length and lexicographical order
 */
final class ResultsComparator implements Comparator<String> {

    private static final ResultsComparator INSTANCE = new ResultsComparator();

    private ResultsComparator() {
    }

    public static Comparator<String> getInstance() {
        return INSTANCE;
    }

    public int compare(final String str1, final String str2) {
        final Integer length1 = str1.length();
        final Integer length2 = str2.length();
        final int lengthComparison = length1.compareTo(length2);
        if (lengthComparison == 0) {
            return -str1.compareTo(str2);
        }
        return -lengthComparison;
    }
}