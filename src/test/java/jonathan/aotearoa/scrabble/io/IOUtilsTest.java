package jonathan.aotearoa.scrabble.io;

import jonathan.aotearoa.scrabble.util.IOUtils;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IOUtilsTest {

    @Test
    public void checkNumberOfLines() throws Exception {
        final LineCounter lineCounter = new LineCounter();
        IOUtils.readLines(IOUtils.SOWPODS_RESOURCE_NAME, lineCounter);
        final int expected = 267751;
        final int actual = lineCounter.getLineCount();
        assertEquals(expected, actual);
    }

    private static class LineCounter implements Consumer<String> {

        private int lineCount = 0;

        @Override
        public void accept(final String line) {
            lineCount++;
        }

        public int getLineCount() {
            return lineCount;
        }
    }
}