package jonathan.aotearoa.scrabble.data;

import jonathan.aotearoa.scrabble.util.IOUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TrieBuilderTest {

    @Test
    public void buildTrie() throws Exception {
        // Given
        final TrieBuilder trieBuilder = new TrieBuilder();
        // When
        IOUtils.readLines(IOUtils.SOWPODS_RESOURCE_NAME, trieBuilder);
        final TrieNode rootNode = trieBuilder.getRootNode();
        // Then
        assertNotNull(rootNode);
    }
}