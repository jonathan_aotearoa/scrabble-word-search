package jonathan.aotearoa.scrabble.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TrieNodeTest {

    @Test
    public void initialState() {
        final TrieNode node = new TrieNode();
        assertFalse(node.isWordEnd());
        assertNull(node.getWord());
        for (char c = 'a'; c <= 'z'; c++) {
            assertFalse(node.hasChild(c));
            assertNull(node.getChild(c));
        }
    }

    @Test
    public void addChild() {
        final TrieNode node = new TrieNode();
        final char c = 'a';
        final TrieNode child = node.addChild(c);
        assertNotNull(child);
        assertTrue(node.hasChild(c));
    }

    @Test
    public void setWord() {
        final TrieNode node = new TrieNode();
        final String word = "hello";
        node.setWord(word);
        assertTrue(node.isWordEnd());
        assertEquals(word, node.getWord());
    }
}