package jonathan.aotearoa.scrabble.data;

import jonathan.aotearoa.scrabble.util.IOUtils;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DictionaryTest {

    @Test
    public void buildDictionary() throws Exception {
        // Given
        final DictionaryBuilder dictionaryBuilder = new DictionaryBuilder();
        // When
        IOUtils.readLines(IOUtils.SOWPODS_RESOURCE_NAME, dictionaryBuilder);
        Map<String, Set<String>> dictionary = dictionaryBuilder.getDictionary();
        // Then
        assertNotNull(dictionary);
        assertEquals(237739, dictionary.size());
        assertEquals(Set.of("bac", "cab"), dictionary.get("abc"));
    }
}