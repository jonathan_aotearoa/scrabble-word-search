package jonathan.aotearoa.scrabble.search;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class SearchTest {

    private Search search;

    @BeforeEach
    public void before() throws IOException {
        search = getSearch();
    }

    public abstract Search getSearch() throws IOException;

    @Test
    public void multipleSearches() {
        final int nSearches = 100;
        final long[] results = new long[nSearches];
        for (int i = 0; i < nSearches; i++) {
            results[i] = search(false, false);
        }
        final long[] typicalResults = Arrays.copyOfRange(results, 9, 90);
        final OptionalDouble average = Arrays.stream(typicalResults).average();
        System.out.println("Average search time: " + average.getAsDouble() + "ms");
    }

    @Test
    public void singleSearch() {
        search(true, true);
    }

    public long search(final boolean printInfo, final boolean assertResults) {
        final String searchTerm = "ocdfaestrghling";
        final StopWatch stopWatch = StopWatch.createStarted();
        final List<String> results = search.search(searchTerm, 2);
        final long ms = stopWatch.getTime();
        if (printInfo) {
            System.out.printf("%s search time: %d ms%n", search.type.searchName, ms);
            System.out.printf("Word count: %d%n", results.size());
        }
        if (assertResults) {
            assertEquals(8491, results.size());
        }
        return ms;
    }
}