package jonathan.aotearoa.scrabble.search;

import java.io.IOException;

import static jonathan.aotearoa.scrabble.search.SearchFactory.SOWPODS_SEARCH_FACTORY;

public class BruteForceSearchTest extends SearchTest {

    @Override
    public Search getSearch() throws IOException {
        return SOWPODS_SEARCH_FACTORY.newSearch(SearchType.BRUTE_FORCE);
    }
}