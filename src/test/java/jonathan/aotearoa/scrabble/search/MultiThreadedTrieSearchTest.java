package jonathan.aotearoa.scrabble.search;

import java.io.IOException;

import static jonathan.aotearoa.scrabble.search.SearchFactory.SOWPODS_SEARCH_FACTORY;

public class MultiThreadedTrieSearchTest extends SearchTest {

    @Override
    public Search getSearch() throws IOException {
        return SOWPODS_SEARCH_FACTORY.newSearch(SearchType.MULTI_THREADED_TRIE);
    }
}
