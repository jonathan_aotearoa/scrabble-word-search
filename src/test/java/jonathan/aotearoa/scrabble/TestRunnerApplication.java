package jonathan.aotearoa.scrabble;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import jonathan.aotearoa.scrabble.search.Search;
import jonathan.aotearoa.scrabble.search.SearchType;
import org.apache.commons.lang3.time.StopWatch;

import static jonathan.aotearoa.scrabble.search.SearchFactory.SOWPODS_SEARCH_FACTORY;

public class TestRunnerApplication extends Application {

    private Search dictionarySearch;
    private Search trieSearch;
    private Search multithreadedTrieSearch;

    @Override
    public void start(final Stage primaryStage) throws Exception {
        dictionarySearch = SOWPODS_SEARCH_FACTORY.newSearch(SearchType.DICTIONARY);
        trieSearch = SOWPODS_SEARCH_FACTORY.newSearch(SearchType.TRIE);
        multithreadedTrieSearch = SOWPODS_SEARCH_FACTORY.newSearch(SearchType.MULTI_THREADED_TRIE);
        final GridPane gridPane = createUI();
        final Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public GridPane createUI() {
        final GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10));
        // controls
        final TextField searchTermTextField = new TextField();
        final ComboBox<SearchType> searchTypeComboBox = new ComboBox<>();
        searchTypeComboBox.getItems().addAll(
                SearchType.DICTIONARY,
                SearchType.TRIE,
                SearchType.MULTI_THREADED_TRIE
        );
        final Spinner<Integer> searchCountSpinner = new Spinner<>(10, 1000, 100, 10);
        final Button searchButton = new Button("Search");
        final TextArea outputTextArea = new TextArea();
        outputTextArea.setEditable(false);
        // actions
        searchButton.setOnAction(event -> {
            final SearchType searchType = searchTypeComboBox.getValue();
            final int searchCount = searchCountSpinner.getValue();
            final String searchTerm = searchTermTextField.getText();
            Search search = switch (searchType) {
                case DICTIONARY -> dictionarySearch;
                case TRIE -> trieSearch;
                case MULTI_THREADED_TRIE -> multithreadedTrieSearch;
                default -> null;
            };
            if (search == null) {
                return;
            }
            final StopWatch stopWatch = StopWatch.createStarted();
            for (int i = 0; i < searchCount; i++) {
                search.search(searchTerm, Search.MIN_WORD_LENGTH);
            }
            stopWatch.stop();
            final String message = String.format("Executed %s searches in %sms", searchCount, stopWatch.getTime());
            outputTextArea.setText(message);
        });
        // add controls to pane
        gridPane.add(searchTermTextField, 0, 0, 3, 1);
        gridPane.add(searchTypeComboBox, 0, 1);
        gridPane.add(searchCountSpinner, 1, 1);
        gridPane.add(searchButton, 2, 1);
        gridPane.add(outputTextArea, 0, 2, 3, 1);
        return gridPane;
    }

    public static void main(String[] args) {
        launch(args);
    }
}